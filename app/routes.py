from app import db, app, oauth
from app.forms import LoginForm, RegisterForm
from app.models import User, weatherFLWTC
from flask_login import LoginManager, UserMixin, login_user, logout_user, login_required, current_user


from API.HKOAPI.FLW.flw import stats, GS, TI, FDW, FP, FD, O, UT
from flask import flash, render_template, redirect, url_for, request, session , Flask
from flask_mysqldb import MySQL
import MySQLdb.cursors
import re
from API.HKOAPI.FLW.flw import stats, GS, TI, FDW, FP, FD, O, UT


from app import app

mysql = MySQL(app)
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"



@app.route('/explore')
def explore():
    return render_template('explore.html')



@app.route('/test')
def test1():
    return render_template('test.html')

@app.route('/hiking1')
def hiking1():
    return render_template('hiking1.html')

@app.route('/main')
def test():
    return render_template('main.html')

@app.route('/login-page', methods=['GET','POST'])
def login_page():
    if current_user.is_authenticated:
        return redirect(url_for('dashboard'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password1.data):
            flash('Invalid username or password')
            return redirect(url_for('login_page'))
        login_user(user)
        return redirect(url_for('dashboard'))
    return render_template('login-page.html', form=form)

@login_manager.user_loader
def load_user(id):
    return User.query.get(int(id))


@app.route('/dashboard', methods=['GET','POST'])
@login_required
def dashboard():
    googlename = dict(session).get('email', None)
    GSit = db.session.query(weatherFLWTC.generalSituation).order_by(weatherFLWTC.id.desc()).first()
    UTime = db.session.query(weatherFLWTC.updateTime).order_by(weatherFLWTC.id.desc()).first()
    return render_template('dashboard.html', GSit=GSit, UTime=UTime, googlename=googlename)

@app.route('/navbar')
def navbar():
    return render_template('navbar.html')

@app.route('/test1')
def navbar2():
    return render_template('navbar1.html')

@app.route('/test-form')
def test_form():
    form = RegisterForm()
    return render_template('test_register.html', form=form)

@app.route('/googlelogin')
def googlelogin():
    google = oauth.create_client('google')
    redirect_uri = url_for('googleauthorize', _external=True)
    return google.authorize_redirect(redirect_uri)

@app.route('/googleauthorize')
def googleauthorize():
    google = oauth.create_client('google')
    token = google.authorize_access_token()
    resp = google.get('userinfo')
    resp.raise_for_status()
    user_info = resp.json()
    # do something with the token and profile
    session['email'] = user_info['email']
    return redirect('/')

@app.route('/googlelogout')
def googlelogout():
    for key in list(session.keys()):
        session.pop(key)
    return redirect('/')


 #test mysql 
@app.route('/')
@app.route('/login', methods =['GET', 'POST'])
def login():
    msg = ''
    if request.method == 'POST' and 'username' in request.form and 'password' in request.form:
        username = request.form['username']
        password = request.form['password']
        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute('SELECT * FROM accounts WHERE username = % s AND password = % s', (username, password, ))
        account = cursor.fetchone()
        if account:
            session['loggedin'] = True
            session['id'] = account['id']
            session['username'] = account['username']
            msg = 'Logged in successfully !'
            return render_template('index.html', msg = msg)
        else:
            msg = 'Incorrect username / password !'
    return render_template('login.html', msg = msg)
 
@app.route('/logout')
def logout():
   session.pop('loggedin', None)
   session.pop('id', None)
   session.pop('username', None)
   return redirect(url_for('login'))
 
@app.route('/register', methods =['GET', 'POST'])
def register():
    msg = ''
    if request.method == 'POST' and 'username' in request.form and 'password' in request.form and 'email' in request.form and 'address' in request.form and 'city' in request.form and 'country' in request.form and 'postalcode' in request.form and 'organisation' in request.form:
        username = request.form['username']
        password = request.form['password']
        email = request.form['email']
        organisation = request.form['organisation'] 
        address = request.form['address']
        city = request.form['city']
        state = request.form['state']
        country = request.form['country']   
        postalcode = request.form['postalcode']
        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute('SELECT * FROM accounts WHERE username = % s', (username, ))
        account = cursor.fetchone()
        if account:
            msg = 'Account already exists !'
        elif not re.match(r'[^@]+@[^@]+\.[^@]+', email):
            msg = 'Invalid email address !'
        elif not re.match(r'[A-Za-z0-9]+', username):
            msg = 'name must contain only characters and numbers !'
        else:
            cursor.execute('INSERT INTO accounts VALUES (NULL, % s, % s, % s, % s, % s, % s, % s, % s, % s)', (username, password, email, organisation, address, city, state, country, postalcode, ))
            mysql.connection.commit()
            msg = 'You have successfully registered !'
    elif request.method == 'POST':
        msg = 'Please fill out the form !'
    return render_template('register.html', msg = msg)
 
 
@app.route("/index")
def index():
    if 'loggedin' in session:
        return render_template("index.html")
    return redirect(url_for('login'))
 
 
@app.route("/display")
def display():
    if 'loggedin' in session:
        cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
        cursor.execute('SELECT * FROM accounts WHERE id = % s', (session['id'], ))
        account = cursor.fetchone()   
        return render_template("display.html", account = account)
    return redirect(url_for('login'))
 
@app.route("/update", methods =['GET', 'POST'])
def update():
    msg = ''
    if 'loggedin' in session:
        if request.method == 'POST' and 'username' in request.form and 'password' in request.form and 'email' in request.form and 'address' in request.form and 'city' in request.form and 'country' in request.form and 'postalcode' in request.form and 'organisation' in request.form:
            username = request.form['username']
            password = request.form['password']
            email = request.form['email']
            organisation = request.form['organisation'] 
            address = request.form['address']
            city = request.form['city']
            state = request.form['state']
            country = request.form['country']   
            postalcode = request.form['postalcode']
            cursor = mysql.connection.cursor(MySQLdb.cursors.DictCursor)
            cursor.execute('SELECT * FROM accounts WHERE username = % s', (username, ))
            account = cursor.fetchone()
            if account:
                msg = 'Account already exists !'
            elif not re.match(r'[^@]+@[^@]+\.[^@]+', email):
                msg = 'Invalid email address !'
            elif not re.match(r'[A-Za-z0-9]+', username):
                msg = 'name must contain only characters and numbers !'
            else:
                cursor.execute('UPDATE accounts SET  username =% s, password =% s, email =% s, organisation =% s, address =% s, city =% s, state =% s, country =% s, postalcode =% s WHERE id =% s', (username, password, email, organisation, address, city, state, country, postalcode, (session['id'], ), ))
                mysql.connection.commit()
                msg = 'You have successfully updated !'
        elif request.method == 'POST':
            msg = 'Please fill out the form !'
        return render_template("update.html", msg = msg)
    return redirect(url_for('login'))
 


@app.route('/mainpage', methods=["POST", "GET"])
def indexorginial():    # put application's code here
    r = stats()
    if r == 200:
        apical_to_create = weatherFLWTC(generalSituation=GS(),
                                        tcInfo=TI(),
                                        fireDangerWarning=FDW(),
                                        forecastPeriod=FP(),
                                        forecastDesc=FD(),
                                        outlook=O(),
                                        updateTime=UT())
        db.session.add(apical_to_create)
        db.session.commit()
    GSit = db.session.query(weatherFLWTC.generalSituation).order_by(weatherFLWTC.id.desc()).first()
    UTime = db.session.query(weatherFLWTC.updateTime).order_by(weatherFLWTC.id.desc()).first()
    return render_template('index-original.html', GSit=GSit, UTime=UTime)

@app.errorhandler(404)
def page_not_found(e):
    return render_template("404.html"), 404

@app.errorhandler(500)
def page_not_found(e):
    return render_template("500.html"), 500


@app.route('/registerorginial', methods=['GET','POST'])
def register_page():
    form = RegisterForm()
    if form.validate_on_submit():
        user_to_create = User(username=form.username.data,
                              email_address=form.email_address.data)
        user_to_create.set_password(form.password1.data)
        db.session.add(user_to_create)
        db.session.commit()
        return redirect(url_for('index'))
    if form.errors != {}: #if there are not errors form the validation
        for err_msg in form.errors.values():
            flash(f'There was an error with creating a user:{err_msg}',category='danger')
    return render_template('register.html', form=form)


@app.route('/logoutorginial', methods=['GET', 'POST'])
@login_required
def logoutorginial():
    logout_user()
    return 'You are now logged out!'
