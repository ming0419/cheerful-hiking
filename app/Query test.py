from flask import flash, render_template, redirect, url_for, request
from app import db, app

from app.forms import LoginForm, RegisterForm
from app.models import User, weatherFLWTC
from flask_login import LoginManager, UserMixin, login_user, logout_user, login_required, current_user


from API.HKOAPI.FLW.flw import stats, GS, TI, FDW, FP, FD, O, UT

r1 = db.session.query(weatherFLWTC.updateTime).order_by(weatherFLWTC.id.desc()).first()

print(r1)